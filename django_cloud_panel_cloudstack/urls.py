from django.urls import path

from . import views

urlpatterns = [
    path("virtual-machines", views.list_virtual_machines),
    path("virtual-machines/create", views.modal_virtual_machine_create),
    path("virtual-machine/<str:virtual_machine>", views.edit_virtual_machine),
    path(
        "virtual-machine/<str:virtual_machine>/delete",
        views.modal_virtual_machine_delete,
    ),
    path("kubernetes", views.config_kubernetes),
]
