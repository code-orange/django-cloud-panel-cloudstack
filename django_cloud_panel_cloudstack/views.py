from django.http import HttpResponse
from django.template import loader
from django.utils.translation import gettext as _

from django_cloud_panel_main.django_cloud_panel_main.views import get_nav
from django_session_ldap_attributes.django_session_ldap_attributes.decorators import (
    admin_group_required,
)


@admin_group_required("COMPUTECLOUD")
def list_virtual_machines(request):
    template = loader.get_template(
        "django_cloud_panel_cloudstack/list_virtual_machines.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Compute Cloud")
    template_opts["content_title_sub"] = _("List Virtual Machines")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("COMPUTECLOUD")
def edit_virtual_machine(request, virtual_machine):
    template = loader.get_template(
        "django_cloud_panel_cloudstack/edit_virtual_machine.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Compute Cloud")
    template_opts["content_title_sub"] = _("Edit Virtual Machine")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("COMPUTECLOUD")
def modal_virtual_machine_delete(request, virtual_machine):
    template = loader.get_template(
        "django_cloud_panel_cloudstack/modal_virtual_machine_delete.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Compute Cloud")
    template_opts["content_title_sub"] = (
        _("Delete Virtual Machine") + " " + virtual_machine
    )

    template_opts["virtual_machine"] = virtual_machine

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("COMPUTECLOUD")
def modal_virtual_machine_create(request):
    template = loader.get_template(
        "django_cloud_panel_cloudstack/modal_virtual_machine_create.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Compute Cloud")
    template_opts["content_title_sub"] = _("Create Virtual Machine")

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("COMPUTECLOUD")
def config_kubernetes(request):
    template = loader.get_template(
        "django_cloud_panel_cloudstack/config_kubernetes.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Compute Cloud")
    template_opts["content_title_sub"] = _("Kubernetes")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))
